﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Portfolio_AWR.App_Start
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                //"~/Content/js/jquery-1.11.0.min.js",
                "~/Content/js/owl.carousel.js",
                "~/Content/js/jquery.fitvids.js",
                //"~/Content/js/jquery.fancybox.pack.js?v=2.1.5",
                "~/Content/js/retina.js",
                "~/Content/js/jquery.scrollToTop.min.js",
                "~/Content/js/jquery.fs.wallpaper.js",
                "~/Content/js/jquery.easing.1.3.js",
                "~/Content/js/jquery.plusanchor.js",
                "~/Content/js/jquery.knob.js",
                //"~/Content/js/app.js",
                "~/Content/js/wow.min.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css_main").Include(
                "~/Content/css/main.css",
                "~/Content/css/owl.carousel.css",
                "~/Content/css/owl.theme.css",
                "~/Content/css/fontello.css",
                //"~/Content/css/jquery.fancybox.css?v=2.1.5",
                //"~/Content/css/jquery.fs.wallpaper.css",
                "~/Content/css/animate.css"
            ));

        }
    }
}