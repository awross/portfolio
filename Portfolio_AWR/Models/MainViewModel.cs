﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Portfolio_AWR.Models
{
    public class MainViewModel
    {
        public string FullName { get; set; }
        public DateTime LastHire { get; set; }
        public int Years_Current { get; set; }
        public int Months_Current { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Message { get; set; }

        public MainViewModel(string name, string hireDate)
        {
            FullName = name;
            LastHire = DateTime.Parse(hireDate);
            DateTime n = DateTime.Now;
            Months_Current = Convert.ToInt32(Math.Round(n.Subtract(LastHire).Days / (365.25 / 12)));
            Years_Current = Convert.ToInt32(Math.Floor((decimal)(Months_Current / 12)));
            Months_Current = Months_Current % 12;
        }

        public string Current_Str
        {
            get
            {
                if (Years_Current == 0)
                {
                    return String.Format("{0} {1}", Months_Current, (Months_Current == 1 ? "month" : "months"));
                }
                else
                {
                    return String.Format("{0} {1}, {2} {3}", Years_Current, (Years_Current == 1 ? "year" : "years"), Months_Current, (Months_Current == 1 ? "month" : "months"));
                }
            }
        }
    }
}