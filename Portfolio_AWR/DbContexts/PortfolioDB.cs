﻿using Portfolio_AWR.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Portfolio_AWR.DbContexts
{
    public class PortfolioDB : DbContext
    {
        public PortfolioDB() : base("DefaultConnection")
        {

        }

        public DbSet<Message> Messages { get; set; }
    }
}