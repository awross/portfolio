﻿using Portfolio_AWR.DbContexts;
using Portfolio_AWR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portfolio_AWR.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            MainViewModel vm = new MainViewModel(Portfolio_AWR.Properties.Settings.Default.FullName, "6/1/2021");
            return View(vm);
        }

        [HttpPost]
        public ActionResult LeaveMessage(string Name, string Email, string Message)
        {
            Message m = new Message();
            m.Name = Name;
            m.Email = Email;
            m.Note = Message;

            PortfolioDB db = new PortfolioDB();
            db.Messages.Add(m);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}